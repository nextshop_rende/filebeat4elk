FROM docker.elastic.co/beats/filebeat:5.6.0
COPY config/filebeat.yml /usr/share/filebeat/filebeat.yml
USER root
#RUN chmod go-w /usr/share/filebeat/filebeat.yml
RUN chown filebeat /usr/share/filebeat/filebeat.yml
USER filebeat
